/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment.pkg1;

import java.util.ArrayList;


public class PointsReport extends Report {
    private int p_total;
    private int low;
    private int medium;
    private int high;
    private int suf;
    private ArrayList<Card> data;

    public PointsReport(ArrayList<Card> data) {
        super(data);
        this.data = data;

    }
    
    // Builds points report
    // and then prints it to screen
    public void getPointsReport(){
        int[] t = new int[4];
        t = this.calcThresholds(this.data);
        p_total = t[0];
        low = t[1];
        medium = t[2];
        high = t[3];
        suf = t[4];
        
       System.out.println("POINTS REPORT");
       System.out.println("=====================");
       System.out.println("Generated: " + super.created);      
       System.out.println();
       System.out.println("High:      " + high);
       System.out.println("Medium:    " + medium);
       System.out.println("Low:       " + low);
       System.out.println();
       System.out.println("TOTAL POINTS:    " + p_total);
       System.out.println("TOTAL FEES:      " + "$"+(suf * 25));
       System.out.println();
       System.out.println();
       System.out.println("=====================");

    
    };
    
    // Method for returning an array of
    // ints that represent the points
    // thresholds.
    public int[] calcThresholds(ArrayList<Card> list) {
        
        int pointsTotal = 0;
        int low = 0;
        int medium = 0;
        int high = 0;
        int suf = 0;
        int[] result = new int[5];
   
        for (int i = 0; i < list.size(); i++) {
            
            Card c = new Card();
            c = list.get(i);
            
            if(c.toString() == "Premium") {
                suf++;
            }
            
            else if (c.points < 500) {
                low++;  
                pointsTotal = pointsTotal + c.points;

            } 
            
            else if (c.points >= 500 && c.points < 2000) {
                medium++;
                pointsTotal = pointsTotal + c.points;

            } 
            
            else if (c.points >= 2000) {
                high++;
                pointsTotal = pointsTotal + c.points;

            }           
        }
        
        result[0] = pointsTotal;
        result[1] = low;
        result[2] = medium;
        result[3] = high;
        result[4] = suf;
        
        return result;
        
    }    
}
