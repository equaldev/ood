/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment.pkg1;

import java.util.ArrayList;


public class CategoryPurchaseReport extends Report {
    private int f_total;
    private int ha_total;
    private int he_total;
    private int p_total;
    private int g_total;
    
    private ArrayList<Purchase> data;

    public CategoryPurchaseReport(ArrayList<Purchase> data) {
        super(data);
        this.data = data;
    }

 
 
    public void getSalesReport(){
         
        int[] t = this.calcTotals(data);
        f_total = t[0];
        ha_total = t[1];
        he_total = t[2];
        p_total = t[3];
        g_total = t[4];
        int to = (f_total + ha_total + he_total + p_total + g_total);
        
       System.out.println("CATEGORY SALES REPORT");
       System.out.println("=====================");
       System.out.println("Generated: " + super.created);      
       System.out.println();
       System.out.println("Food:        $" + f_total);
       System.out.println("Hardware:    $" + ha_total);
       System.out.println("Health:      $" + he_total);
       System.out.println("Pet:         $" + p_total);
       System.out.println("Garden:      $" + g_total);
       System.out.println();
       System.out.println("Total Sales: $" + to);
       System.out.println();
       System.out.println("=====================");

    
    };
    
    
    
    
    public int[] calcTotals(ArrayList<Purchase> list) {
        
        int foodTotal = 0;
        int hardwareTotal = 0;
        int healthTotal = 0;
        int gardenTotal = 0;
        int petTotal = 0;
        
        
        int[] totals = new int[5];
        
        for (int i = 0; i < list.size(); i++) {

            Purchase b = list.get(i);
            
                if (b.cat.toString() == "Food") {
                    foodTotal = foodTotal + b.cat.amount;
                }

                else if (b.cat.toString() == "Hardware") {
                     hardwareTotal = hardwareTotal+ b.cat.amount;
                }
                else if (b.cat.toString() == "Health") {
                     healthTotal = healthTotal+ b.cat.amount;
                }
                else if (b.cat.toString() == "Pet") {
                     petTotal = petTotal + b.cat.amount;
                }
                else if (b.cat.toString() == "Garden") {
                     gardenTotal = gardenTotal + b.cat.amount;
                }
                
        }
        
        totals[0] = foodTotal;
        totals[1] = hardwareTotal;
        totals[2] = healthTotal;
        totals[3] = petTotal;
        totals[4] = gardenTotal;
        return totals;
        
    }    
        
        
    
}
