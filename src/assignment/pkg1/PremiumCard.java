/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment.pkg1;


public class PremiumCard extends Card {
    private String name;
    private String email;
    private double balance;


    public PremiumCard(String name, String email, double balance, int id, int points) {
        super.modifier = 0.025;
        super.id = id;
        super.points = points;
        this.name = name;
        this.email = email;
        super.balance = balance;
    }

    @Override
    public String toString() {
        return "Premium";
    }
    
    
    
}
