/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment.pkg1;

/**
 *
 * @author dev
 */
public class Card {
    
    int id;
    int points;
    double modifier;
    double balance;
    int signUpFee = 25;

    public Card() {
        this.modifier = 0.01;
    }
    
    
    
    @Override
    public String toString() {
        return "Card";
    }
    
    public void addPoints(int amount) {
        
        if (this.toString() == "Basic") {
//            System.out.println("yeah");
            if (this.balance >= 500) {
                
                this.modifier = 0.02;
            }
            else {
                
                this.modifier = 0.015;
            
            }
            
        }
        
        else if (this.toString() == "Premium") {
               //System.out.println("yeah");
               
            if (this.balance > 1000 && amount > 40) {
               
                this.modifier = 0.03;
            }
            
            else {
                
                this.modifier = 0.025;
            
            }
            
            this.balance = this.balance - this.signUpFee;
        }
        
        
        this.points = this.points + (int)(amount * this.modifier);
    
    }
    
    
    
    public void addBalance(int amount) {
        
        this.balance = this.balance + amount;

    }
    
    
}
