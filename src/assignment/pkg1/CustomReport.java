/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment.pkg1;

import java.util.ArrayList;
import java.util.Scanner;


public class CustomReport extends Report {
    private int[] bounds = new int[2];
    private ArrayList<Card> data;
    private ArrayList<Card> results;
    private int total;

    

    public CustomReport(ArrayList<Card> data) {
        super(data);
        this.data = data;
    }
    
    // Generates the final report.
    public void getCustomReport() {
        bounds = this.getUserBounds();
        System.out.println();

        results = this.filterCards(data, bounds);
        int tCards = 0;
        
        for(int i = 0; i < results.size(); i++) {
            
            tCards++;
            
        }
        // TODO: count how many cards inside current band
        // and format report. - done
        total = this.getTotal(results);
        
        
        
       System.out.println("FILTER CARDS BY POINTS");
       System.out.println("=====================");
       System.out.println("Generated: " + super.created);      
       System.out.println();
       System.out.println("Range:          between " + bounds[0] + " and " + bounds[1] + " points.");
       System.out.println("Cards found:    " + tCards);
       System.out.println();
       System.out.println("Total Points in band: " + total);
       System.out.println();
       System.out.println("=====================");
       System.out.println();
       System.out.println();

    }
    
    // Returns an array of ints. 
    // Gets user input for lower 
    // and upper bound for filtering.
    public int[] getUserBounds() {
        
        Scanner input = new Scanner(System.in);
        int low;
        int high;
        System.out.println("Filter Customers by Points");

        Boolean valid = false;
        do {
            System.out.println("Enter Lower Bound: (Whole Number)");
            while (!input.hasNextInt()) {
                System.out.println("Enter a whole number.");
                input.next(); 
            }
            low = input.nextInt();
            valid = true;
            
        } while (!valid);
        
        do {
            valid = false;
            System.out.println("Enter Upper Bound: (Whole Number)");
            while (!input.hasNextInt()) {
                System.out.println("Enter a whole number.");
                input.next();
            }
            high = input.nextInt();
            valid = true;
            
        } while (!valid);
        
        int[] result = new int [2];
        
        result[0] = low;
        result[1] = high;
        
        return result;
    }

    public String toString() {
        return "Custom";
    }
    
    
    // Sorts through Card data relative
    // to lower and upper bound
    // returns an ArrayList of Cards.
   public ArrayList<Card> filterCards(ArrayList<Card> list, int[] bounds) {
        
        int lBound = bounds[0];
        int uBound = bounds[1];
   
        ArrayList<Card> results = new ArrayList<Card>();
   
        for (int i = 0; i < list.size(); i++) {
            
            Card c = new Card();
            c = list.get(i);
            
            if (c.points >= lBound && c.points <= uBound) {
                results.add(c);
            }             
        }          
        return results;       
    }      
   
   //Iterates through filtered cards
   // and totals all points.
   public int getTotal(ArrayList<Card> list) {
       
       int t = 0;
       
       for (int i = 0; i < list.size(); i++) {
           
            Card c = new Card();
            c = list.get(i);
            t = t + c.points;
        }   
       
       return t;
   }
}
