/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment.pkg1.categories;

/**
 *
 * @author dev
 */
public class Category {
    public String name;
    public int amount;

    @Override
    public String toString() {
        return "Category{" + "name=" + name + ", amount=" + amount + '}';
    }
    
}
