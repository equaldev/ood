/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment.pkg1;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author dev
 */
public class Report  {
    
    private Date now = new Date();
    String created;
    String type;
    private Report report;
    private ArrayList data;


    public Report(ArrayList data) {
        this.created = this.now.toString();
        this.data = data;
    }

    
    // Takes a String from user input 
    // and prints the appropriate report.
    public void printReport(){
//        System.out.print(this.type);
        if (this.type == "Purchase") {
            CategoryPurchaseReport report = new CategoryPurchaseReport(this.data);
            report.getSalesReport();
        }
        else if (this.type == "Card") {
            PointsReport report = new PointsReport(this.data);
            report.getPointsReport();
        }
        else if (this.type == "Custom") {
            CustomReport report = new CustomReport(this.data);
            report.getCustomReport();
        }
        
    }
    
    
}

